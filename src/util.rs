use crate::CpfResult;

use std::ffi::CStr;
use std::os::raw::c_char;

pub struct WinApiFiletime {
    dw_low_date_time: u32,
    dw_high_date_time: u32,
}

impl WinApiFiletime {
    pub fn to_unix_epoch(&self) -> u64 {
        const SECS_BETWEEN_WIN_AND_UNIX_EPOCH: u64 = 11644473600_u64;

        // 100ths of nsecs since 1601/1/1 UTC
        let mut time: u64 = ((self.dw_high_date_time as u64) << 32) | (self.dw_low_date_time as u64);
        time /= 1_000_000_0_u64; // To seconds

        if time < SECS_BETWEEN_WIN_AND_UNIX_EPOCH {
            return 0;
        }

        time - SECS_BETWEEN_WIN_AND_UNIX_EPOCH
    }
}

impl TryFrom<&[u8]> for WinApiFiletime {
    type Error = CpfResult;

    fn try_from(v: &[u8]) -> Result<WinApiFiletime, CpfResult> {
        if v.len() != 8 {
            return Err(CpfResult::WrongSliceLength);
        }

        let dw_low_date_time = from_four_le_bytes(v, 0_usize);
        let dw_high_date_time = from_four_le_bytes(v, 4_usize);

        Ok(Self{ dw_low_date_time, dw_high_date_time })
    }
}

pub trait FromTwoBytes {
    fn from_be_bytes(bytes: &[u8; 2]) -> Self;
    fn from_le_bytes(bytes: &[u8; 2]) -> Self;
}

pub trait FromFourBytes {
    fn from_be_bytes(bytes: &[u8; 4]) -> Self;
    fn from_le_bytes(bytes: &[u8; 4]) -> Self;
}

pub trait FromEightBytes {
    fn from_be_bytes(bytes: &[u8; 8]) -> Self;
    fn from_le_bytes(bytes: &[u8; 8]) -> Self;
}

impl FromTwoBytes for u16 {
    fn from_be_bytes(bytes: &[u8; 2]) -> u16 { u16::from_be_bytes(*bytes) }
    fn from_le_bytes(bytes: &[u8; 2]) -> u16 { u16::from_le_bytes(*bytes) }
}

impl FromFourBytes for u16 {
    fn from_be_bytes(bytes: &[u8; 4]) -> u16 { u16::from_be_bytes(bytes[2..3].try_into().unwrap()) }
    fn from_le_bytes(bytes: &[u8; 4]) -> u16 { u16::from_le_bytes(bytes[0..1].try_into().unwrap()) }
}

impl FromFourBytes for u32 {
    fn from_be_bytes(bytes: &[u8; 4]) -> u32 { u32::from_be_bytes(*bytes) }
    fn from_le_bytes(bytes: &[u8; 4]) -> u32 { u32::from_le_bytes(*bytes) }
}

impl FromEightBytes for u16 {
    fn from_be_bytes(bytes: &[u8; 8]) -> u16 { u16::from_be_bytes(bytes[6..7].try_into().unwrap()) }
    fn from_le_bytes(bytes: &[u8; 8]) -> u16 { u16::from_le_bytes(bytes[0..1].try_into().unwrap()) }
}

impl FromEightBytes for u32 {
    fn from_be_bytes(bytes: &[u8; 8]) -> u32 { u32::from_be_bytes(bytes[4..7].try_into().unwrap()) }
    fn from_le_bytes(bytes: &[u8; 8]) -> u32 { u32::from_le_bytes(bytes[0..3].try_into().unwrap()) }
}

impl FromEightBytes for u64 {
    fn from_be_bytes(bytes: &[u8; 8]) -> u64 { u64::from_be_bytes(*bytes) }
    fn from_le_bytes(bytes: &[u8; 8]) -> u64 { u64::from_le_bytes(*bytes) }
}

pub fn from_two_be_bytes<T: FromTwoBytes, Offset: Into<usize>>(buffer: &[u8], offset: Offset) -> T {
    let off = offset.into();
    T::from_be_bytes(buffer[off..off+2].try_into().unwrap())
}

pub fn from_four_be_bytes<T: FromFourBytes, Offset: Into<usize>>(buffer: &[u8], offset: Offset) -> T {
    let off = offset.into();
    T::from_be_bytes(buffer[off..off+4].try_into().unwrap())
}

pub fn from_eight_be_bytes<T: FromEightBytes, Offset: Into<usize>>(buffer: &[u8], offset: Offset) -> T {
    let off = offset.into();
    T::from_be_bytes(buffer[off..off+8].try_into().unwrap())
}

pub fn from_two_le_bytes<T: FromTwoBytes, Offset: Into<usize>>(buffer: &[u8], offset: Offset) -> T {
    let off = offset.into();
    T::from_le_bytes(buffer[off..off+2].try_into().unwrap())
}

pub fn from_four_le_bytes<T: FromFourBytes, Offset: Into<usize>>(buffer: &[u8], offset: Offset) -> T {
    let off = offset.into();
    T::from_le_bytes(buffer[off..off+4].try_into().unwrap())
}

pub fn from_eight_le_bytes<T: FromEightBytes, Offset: Into<usize>>(buffer: &[u8], offset: Offset) -> T {
    let off = offset.into();
    T::from_le_bytes(buffer[off..off+8].try_into().unwrap())
}

pub fn c_str_to_string(ptr: *const c_char) -> Result<String, CpfResult> {
    let cstr = unsafe { CStr::from_ptr(ptr) };

    match cstr.to_str() {
        Ok(s) => Ok(String::from(s)),
        Err(_) => Err(CpfResult::BadString),
    }
}

pub fn compare_mem(a: &[u8], b: &[u8]) -> bool {
    if a.len() != b.len() {
        false
    } else {
        for (ai, bi) in a.iter().zip(b.iter()) {
            if ai != bi {
                return false;
            }
        }

        true
    }
}

pub fn u8_to_u16_be(bytes: &[u8]) -> Result<Vec<u16>, ()> {
    if bytes.len() % 2 != 0 {
        return Err(());
    }

    let mut shorts = Vec::<u16>::with_capacity(bytes.len() / 2);
    for idx in (0..bytes.len()).step_by(2) {
        let short = ((bytes[idx] as u16) << 8) | (bytes[idx+1] as u16);
        shorts.push(short);
    }

    Ok(shorts)
}

pub fn u8_to_u16_le(bytes: &[u8]) -> Result<Vec<u16>, ()> {
    if bytes.len() % 2 != 0 {
        return Err(());
    }

    let mut shorts = Vec::<u16>::with_capacity(bytes.len() / 2);
    for idx in (0..bytes.len()).step_by(2) {
        let short = bytes[idx] as u16 | ((bytes[idx+1] as u16) << 8);
        shorts.push(short);
    }

    Ok(shorts)
}

