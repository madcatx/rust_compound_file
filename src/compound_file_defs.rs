use crate::Clsid;
use crate::util::WinApiFiletime;

use std::mem::size_of;

pub type Difat = Vec<usize>;
pub type Fat = Vec<usize>;
pub type Minifat = Vec<usize>;

/// Byte order of the compound file
#[repr(u16)]
pub enum ByteOrder {
    LittleEndian = 0xFFFE,
    BigEndian = 0xFEFF,
}

/// Known versions of the compound file
#[derive(PartialEq)]
pub enum FileFormatVersion {
    Standard,
    Mso2010,
    MsnInfo,
    Unknown
}

/// Type of the Entry
#[repr(u8)]
#[derive(Debug, PartialEq)]
pub enum EntryObjectType {
    /// Unknown or unallocated CompoundFileEntry object
    UnknownUnallocated = 0x0,
    /// Entry is a StorageObject - a directory
    StorageObject = 0x1,
    /// Entry is a StreamObject - a file
    StreamObject = 0x2,
    /// Entry is the root directory
    RootStorageObject = 0x5,
}

#[repr(u8)]
pub enum EntryColor {
    Red = 0,
    Black = 1,
}

/// An entry in the compound file directory structure. The entry may represent
/// either a directory (StorageObject) or a a file (StreamObject).
/// This structure must be 128 bytes long
pub struct Entry {
    /// Name of the entry as null-terminated UTF-16 string
    pub name: [u16; 32],
    /// Length of the name in bytes(!), not UTF-16 code points
    pub name_len: u16,
    /// Type of the entry. See DirectoryEntryObjectType for details
    pub object_type: EntryObjectType,
    /// Color flag of the entry. Directory structure is internally represented by a red-black tree.
    pub color_flag: EntryColor,
    /// ID of the left sibling entry. Siblings are entries that exist in the same level
    /// in the directory structure. If there are file or directory entries in a given
    /// directory entry, they will be represented as siblings.
    pub left_sibling_id: u32,
    /// ID of the right sibling entry. Siblings are entries that exist in the same level
    /// in the directory structure. If there are file or directory entries in a given
    /// directory entry, they will be represented as siblings.
    pub right_sibling_id: u32,
    /// ID of the child entry. Child entry is the entry that represents the content
    /// of a directory entry.
    pub child_id: u32,
    /// CLSID. May be used to determine the kind of content represented by the given entry.
    pub clsid: Clsid,
    /// User-defined state bits
    pub state_bits: u32,
    /// Time when the entry was created
    pub creation_time: WinApiFiletime,
    /// Time when the entry was last modified
    pub modified_time: WinApiFiletime,
    /// Number of the sector where the data associated with this entry begins.
    /// Valid only for files - StreamObjects
    pub starting_sector_location: u32,
    /// Length of the associated data stream. Valid only for files - StreamObjects
    pub stream_size: u64,
}

/// Typedef for the array that represents the first 109 DIFAT entries that fit in the CompoundFileHeader
pub type DifatFirstPiece = [u32; 109];

/// Typedef for the array that holds the file's signature
pub type Signature = [u8; 8];

/// Header of the compound file. Used to determine how to read the file.
/// This structure must be 512 bytes long
pub struct Header {
    /// File signature. Must be equal to the prescribed signature
    pub signature: Signature,
    /// CLSID. May be used to determine the kind of content represented by the file.
    pub clsid: Clsid,
    pub minor_version: u16,
    pub major_version: u16,
    /// Byte order of the file. Must be set to LittleEndian
    pub byte_order: ByteOrder,
    /// Size of the sector as a power of two: sector_size = 2^sector_shift
    pub sector_shift: u16,
    /// Size of the mini sector as a power of two: mini_sector_size = 2^mini_sector_shift
    pub mini_sector_shift: u16,
    pub reserved_1: u16,
    pub reserved_2: u32,
    /// Number of sectors that contain CompoundFileEntry data
    pub num_dir_sectors: u32,
    /// Number of sectors that contain the FAT
    pub num_fat_sectors: u32,
    /// First sector of the FAT chain that contains CompoundFileEntry data
    pub first_directory_sector_location: u32,
    /// Used for transactional access to the file - unused by us
    pub transaction_signature_number: u32,
    /// Any stream larger than this size will be stored in regular FAT sectors.
    /// Other streams will use the MiniFAT sectors.
    pub mini_stream_cutoff_size: u32,
    /// First sector of the MiniFAT sector chain in the regular FAT
    pub first_mini_fat_sector_location: u32,
    /// Number of regular FAT sectors used by the MiniFAT
    pub num_mini_fat_sectors: u32,
    /// First sector of the DIFAT chain in the regular FAT.
    /// This value must be set to END_OF_CHAIN unless the DIFAT is larger
    /// than 109 entires and does not fit in the header
    pub first_difat_sector_location: u32,
    /// Number of regular FAT sectors used by DIFAT. Non-zero only if the DIFAT
    /// does not fit in the header.
    pub num_difat_sectors: u32,
    /// First piece of DIFAT. May contain up to 109 entries and is not terminated
    /// by END_OF_CHAIN value
    pub difat_first_piece: DifatFirstPiece,
}

/// Number of DIFAT entries that fit in the header
pub const DIFAT_IN_HEADER_SIZE: usize = size_of::<DifatFirstPiece>() / size_of::<u32>();
/// Maximum ID of a regular stream
pub const MAX_STREAM_ID: usize = 0xFFFF_FFFA;
/// Denotes maximum sector number that can refer to a data sector
pub const FAT_MAX_SECTOR_NUMBER: usize = 0xFFFF_FFFA;
/// Denotes unused sector in FAT
pub const FAT_SECTOR_UNUSED: usize = 0xFFFF_FFFF;
/// Denotes end of a sector chain in FAT
pub const FAT_SECTOR_END_OF_CHAIN: usize = 0xFFFF_FFFE;
/// Denotes a sector used for FAT
pub const FAT_SECTOR_USED_FOR_FAT: usize = 0xFFFF_FFFD;
/// Denotes a sector used for DIFAT
pub const FAT_SECTOR_USED_FOR_DIFAT: usize = 0xFFFF_FFFC;
