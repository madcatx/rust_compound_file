use crate::{Clsid, CpfResult};
use crate::compound_file_defs::*;
use crate::util::*;

use std::cell::RefCell;
use std::mem::size_of;
use std::rc::{Rc, Weak};

const STREAM_ID_NOSTREAM: u32 = 0xFFFF_FFFF;

/// Internal collection of information needed to read through the compound file
struct WalkingInfo<'a> {
    /// The FAT table
    fat: &'a Fat,
    /// Size of the FAT sector
    sector_size: usize,
    /// Number of the first sector of FAT chain that contains Entry data
    first_directory_sector_location: u32,
}

/// Offsets that point to individual fields of the Entry when it is represented as a raw array of bytes
#[repr(usize)]
enum EntryOffsets {
    Name = 0,
    NameLength = 64,
    ObjectType = 66,
    ColorFlag = 67,
    LeftSiblingId = 68,
    RightSiblingId = 72,
    ChildId = 76,
    Clsid = 80,
    StateBits = 96,
    CreationTime = 100,
    ModifiedTime = 108,
    StartingSectorLocation = 116,
    StreamSize = 120,
}
impl Into<usize> for EntryOffsets {
    fn into(self) -> usize { self as usize }
}

/// Offsets that point to individual fields of the Header when it is represented as a raw array of bytes
#[repr(usize)]
enum HeaderOffsets {
    Signature = 0,
    Clsid = 8,
    MinorVersion = 24,
    MajorVersion = 26,
    ByteOrder = 28,
    SectorShift = 30,
    MiniSectorShift = 32,
    Reserved1 = 34,
    Reserved2 = 36,
    NumDirSectors = 40,
    NumFatSectors = 44,
    FirstDirectorySectorLocation = 48,
    TransactionSignatureNum = 52,
    MiniStreamCutoffSize = 56,
    FirstMiniFatSectorLocation = 60,
    NumMiniFatSectors = 64,
    FirstDifatSectorLocation = 68,
    NumDifatSectors = 72,
    Difat = 76,
}
impl Into<usize> for HeaderOffsets {
    fn into(self) -> usize { self as usize }
}

/// File signature. Every valid compound file must begin with this signature
static COMPOUND_FILE_SIGNATURE: &'static [u8; 8] = &[0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1];

fn buffer_piece(buffer: &[u8], offset: usize, length: usize) -> Result<&[u8], CpfResult> {
    if buffer.len() < offset + length {
        return Err(CpfResult::BufferTooSmall);
    }

    Ok(&buffer[offset..offset+length])
}

fn build_directory_tree(buffer: &[u8], root_entry: &Entry, walk_info: &WalkingInfo) -> Result<Rc<RefCell<Directory>>, CpfResult> {
    if root_entry.object_type != EntryObjectType::RootStorageObject {
        return Err(CpfResult::InvalidEntryObjectType);
    }

    let dir = directory_from_entry(&root_entry, None)?;
    if root_entry.child_id != STREAM_ID_NOSTREAM {
        let child_entry = read_entry(buffer, root_entry.child_id, walk_info)?;
        descend_next(buffer, &child_entry, dir.clone(), walk_info, 0)?;
    }

    Ok(dir)
}

fn descend_next(buffer: &[u8], entry: &Entry, parent_dir: Rc<RefCell<Directory>>, walk_info: &WalkingInfo, level: usize) -> Result<(), CpfResult> {
    match entry.object_type {
        EntryObjectType::StorageObject => {
            let dir = directory_from_entry(entry, Some(parent_dir.clone()))?;
            parent_dir.borrow_mut().subdirectories.push(dir.clone());

            if entry.child_id != STREAM_ID_NOSTREAM {
                let child_entry = read_entry(buffer, entry.child_id, walk_info)?;
                descend_next(buffer, &child_entry, dir, walk_info, level + 1)?;
            }
        },
        EntryObjectType::StreamObject => parent_dir.borrow_mut().files.push(file_from_entry(entry, parent_dir.clone())?),
        _ => return Err(CpfResult::InvalidEntryObjectType),
    };

    if entry.left_sibling_id != STREAM_ID_NOSTREAM {
        let sibling_entry = read_entry(buffer, entry.left_sibling_id, walk_info)?;
        descend_next(buffer, &sibling_entry, parent_dir.clone(), walk_info, level)?;
    }

    if entry.right_sibling_id != STREAM_ID_NOSTREAM {
        let sibling_entry = read_entry(buffer, entry.right_sibling_id, walk_info)?;
        descend_next(buffer, &sibling_entry, parent_dir.clone(), walk_info, level)?;
    }

    Ok(())
}

fn directory_from_entry(entry: &Entry, parent: Option<Rc<RefCell<Directory>>>) -> Result<Rc<RefCell<Directory>>, CpfResult> {
    assert_ne!(entry.object_type, EntryObjectType::StreamObject);

    let name = string_from_utf16(&entry.name, entry.name_len as usize)?;

    Ok(Rc::<RefCell<Directory>>::new(
        RefCell::<Directory>::new(Directory{
            parent: match parent {
                Some(p) => Some(Rc::downgrade(&p)),
                None => None
            },
            name,
            clsid: entry.clsid.clone(),
            subdirectories: Vec::new(),
            files: Vec::new(),
            timestamp: entry.creation_time.to_unix_epoch(),
        })
    ))
}

fn file_from_entry(entry: &Entry, parent: Rc<RefCell<Directory>>) -> Result<File, CpfResult> {
    assert_eq!(entry.object_type, EntryObjectType::StreamObject);
    assert_eq!(entry.child_id, STREAM_ID_NOSTREAM);

    let name = string_from_utf16(&entry.name, entry.name_len as usize)?;

    Ok(File{
        parent: Rc::downgrade(&parent),
        name,
        size: entry.stream_size,
        first_sector: entry.starting_sector_location as usize,
    })
}

fn location_to_offset(location: usize, chuck_size: usize) -> usize {
    (location + 1) * chuck_size
}

fn make_difat(buffer: &[u8], &difat_first_piece: &DifatFirstPiece, first_difat_sector_location: usize, num_difat_sectors: usize, sector_size: usize) -> Result<Difat, CpfResult> {
    let mut difat: Difat = Vec::with_capacity(num_difat_sectors * sector_size + DIFAT_IN_HEADER_SIZE);

    for idx in 0..DIFAT_IN_HEADER_SIZE {
        let location = difat_first_piece[idx] as usize;
        if location as usize != FAT_SECTOR_UNUSED {
            difat.push(location);
        }
    }

    if first_difat_sector_location as usize == FAT_SECTOR_END_OF_CHAIN {
        return Ok(difat);
    }

    let mut sector_position_counter = 0;
    let mut offset = location_to_offset(first_difat_sector_location as usize, sector_size);
    let mut remaining_sectors = num_difat_sectors;
    while remaining_sectors > 0 {
        if buffer.len() + 4 < offset {
            return Err(CpfResult::InvalidDifatOffset);
        }

        let location = from_four_le_bytes::<u32, usize>(buffer, offset) as usize;
        if location == FAT_SECTOR_END_OF_CHAIN {
            break;
        }

        if sector_position_counter < sector_size - 1 {
            // We are inside a secondary DIFAT sector
            if location != FAT_SECTOR_UNUSED {
                difat.push(location);
            }
            offset += 4;
            sector_position_counter += 1;
        } else {
            // We are at the end of secondary DIFAT sector.
            // The last location in a secondary DIFAT sector points to the next secondary DIFAT sector
            offset = location_to_offset(location, sector_size);
            sector_position_counter = 0;
            remaining_sectors -= 1;
        }
    }

    if remaining_sectors > 0 {
        return Err(CpfResult::DifatCorrupted);
    }

    Ok(difat)
}

fn make_fat(buffer: &[u8], difat: &Difat, sector_size: usize) -> Result<Fat, CpfResult> {
    let mut fat = Vec::<usize>::new();

    for location in difat {
        let offset = location_to_offset(*location, sector_size);
        let piece = buffer_piece(buffer, offset, sector_size)?;

        for idx in (0..sector_size).step_by(4) {
            let loc: usize = from_four_le_bytes::<u32, usize>(&piece, idx) as usize;
            fat.push(loc);
        }
    }

    Ok(fat)
}

fn make_mini_fat(buffer: &[u8], first_mini_fat_sector_location: usize, walk_info: &WalkingInfo) -> Result<Minifat, CpfResult> {
    let mut mini_fat = Vec::<usize>::new();

    let mut location = first_mini_fat_sector_location;
    loop {
        if location == FAT_SECTOR_END_OF_CHAIN {
            break;
        }
        if location >= walk_info.fat.len() {
            return Err(CpfResult::InvalidFatOffset);
        }

        let offset = location_to_offset(location, walk_info.sector_size);
        for idx in (0..walk_info.sector_size).step_by(4) {
            mini_fat.push(from_four_le_bytes::<u32, usize>(buffer, offset + idx) as usize);
        }

        location = walk_info.fat[location];
    }

    Ok(mini_fat)
}

fn mini_location_to_offset(location: usize, chuck_size: usize) -> usize {
    location * chuck_size
}

fn string_from_utf16(arr: &[u16; 32], valid_bytes: usize) -> Result<String, CpfResult> {
    if valid_bytes > 2 {
        let num_chars = ((valid_bytes - 2) / 2) as usize;
        match String::from_utf16(&arr[0..num_chars]) {
            Ok(s) => return Ok(s),
            Err(_) => return Err(CpfResult::InvalidUtf16String),
        }
    } else {
        Ok(String::new())
    }
}

fn read_entry(buffer: &[u8], location: u32, walk_info: &WalkingInfo) -> Result<Entry, CpfResult> {
    let entries_per_sector = walk_info.sector_size / size_of::<Entry>();
    let number_of_sector = location as usize / entries_per_sector;
    let x = sector_number_to_offset(walk_info.first_directory_sector_location as usize, number_of_sector, walk_info)?;
    let offset = x + (location as usize % entries_per_sector) * size_of::<Entry>();
    let piece = buffer_piece(buffer, offset, size_of::<Entry>())?;

    let mut name: [u16; 32] = [0; 32];
    let name_len = from_two_le_bytes::<u16, _>(piece, EntryOffsets::NameLength) as usize;
    if name_len > 64 {
        return Err(CpfResult::EntryNameTooLong);
    }
    if name_len % 2 != 0 {
        return Err(CpfResult::EntryNameHasOddLength);
    }

    match u8_to_u16_le(piece[EntryOffsets::Name as usize..name_len].try_into().unwrap()) {
        Ok(shorts) => name[0..name_len/2].copy_from_slice(shorts.as_slice()),
        Err(_) => return Err(CpfResult::CannotConvertU8ToU16),
    };

    let object_type = match piece[EntryOffsets::ObjectType as usize] {
        0x0 => EntryObjectType::UnknownUnallocated,
        0x1 => EntryObjectType::StorageObject,
        0x2 => EntryObjectType::StreamObject,
        0x5 => EntryObjectType::RootStorageObject,
        _ => return Err(CpfResult::UnknownEntryObjectType),
    };
    let color_flag = match piece[EntryOffsets::ColorFlag as usize] {
        0x0 => EntryColor::Red,
        0x1 => EntryColor::Black,
        _ => return Err(CpfResult::UnknownEntryObjectColor),
    };
    let left_sibling_id = from_four_le_bytes(piece, EntryOffsets::LeftSiblingId);
    let right_sibling_id = from_four_le_bytes(piece, EntryOffsets::RightSiblingId);
    let child_id = from_four_le_bytes(piece, EntryOffsets::ChildId);
    let clsid: Clsid = piece[EntryOffsets::Clsid.into()..(EntryOffsets::Clsid as usize)+16].try_into().unwrap();
    let state_bits = from_four_le_bytes(piece, EntryOffsets::StateBits);
    let creation_time = piece[EntryOffsets::CreationTime.into()..(EntryOffsets::CreationTime as usize)+8].try_into()?;
    let modified_time = piece[EntryOffsets::ModifiedTime.into()..(EntryOffsets::ModifiedTime as usize)+8].try_into()?;
    let starting_sector_location = from_four_le_bytes(piece, EntryOffsets::StartingSectorLocation);
    let stream_size = from_eight_le_bytes(piece, EntryOffsets::StreamSize);

    Ok(Entry{
        name,
        name_len: name_len as u16,
        object_type,
        color_flag,
        left_sibling_id,
        right_sibling_id,
        child_id,
        clsid,
        state_bits,
        creation_time,
        modified_time,
        starting_sector_location,
        stream_size
    })
}

/// Attempts to read compound file header from a binary stream
fn read_header(buffer: &[u8; size_of::<Header>()]) -> Result<Header, CpfResult> {
    let mut signature: [u8; 8] = [0; 8];
    signature.copy_from_slice(&buffer[HeaderOffsets::Signature.into()..COMPOUND_FILE_SIGNATURE.len()]);
    if !compare_mem(&signature[..], COMPOUND_FILE_SIGNATURE) {
        return Err(CpfResult::BadSignature);
    }

    let mut clsid: Clsid = [0; 16];
    clsid.copy_from_slice(&buffer[HeaderOffsets::Clsid.into()..(HeaderOffsets::Clsid as usize)+size_of::<Clsid>()]);

    let minor_version = from_two_le_bytes(buffer, HeaderOffsets::MinorVersion);
    let major_version = from_two_le_bytes(buffer, HeaderOffsets::MajorVersion);
    let byte_order = match from_two_le_bytes(buffer, HeaderOffsets::ByteOrder) {
        0xFFFE => ByteOrder::LittleEndian,
        0xFEFF => return Err(CpfResult::BigEndianNotSupported),
        _ => return Err(CpfResult::UnknownByteOrder),
    };
    let sector_shift = from_two_le_bytes(buffer, HeaderOffsets::SectorShift);
    let mini_sector_shift = from_two_le_bytes(buffer, HeaderOffsets::MiniSectorShift);

    let reserved_1 = from_two_le_bytes(buffer, HeaderOffsets::Reserved1);
    let reserved_2 = from_four_le_bytes(buffer, HeaderOffsets::Reserved2);

    let num_dir_sectors = from_four_le_bytes(buffer, HeaderOffsets::NumDirSectors);
    let num_fat_sectors = from_four_le_bytes(buffer, HeaderOffsets::NumFatSectors);
    let first_directory_sector_location = from_four_le_bytes(buffer, HeaderOffsets::FirstDirectorySectorLocation);
    let transaction_signature_number: u32 = from_four_le_bytes(buffer, HeaderOffsets::TransactionSignatureNum);
    let mini_stream_cutoff_size = from_four_le_bytes(buffer, HeaderOffsets::MiniStreamCutoffSize);
    let first_mini_fat_sector_location = from_four_le_bytes(buffer, HeaderOffsets::FirstMiniFatSectorLocation);
    let num_mini_fat_sectors = from_four_le_bytes(buffer, HeaderOffsets::NumMiniFatSectors);
    let first_difat_sector_location = from_four_le_bytes(buffer, HeaderOffsets::FirstDifatSectorLocation);
    let num_difat_sectors = from_four_le_bytes(buffer, HeaderOffsets::NumDifatSectors);

    let mut difat_first_piece: DifatFirstPiece = [0; 109];
    for idx in 0..difat_first_piece.len() {
        difat_first_piece[idx] = from_four_le_bytes(buffer, HeaderOffsets::Difat as usize + 4 * idx);
    }

    Ok(Header{
        signature,
        clsid,
        minor_version,
        major_version,
        byte_order,
        sector_shift,
        mini_sector_shift,
        reserved_1,
        reserved_2,
        num_dir_sectors,
        num_fat_sectors,
        first_directory_sector_location,
        transaction_signature_number,
        mini_stream_cutoff_size,
        first_mini_fat_sector_location,
        num_mini_fat_sectors,
        first_difat_sector_location,
        num_difat_sectors,
        difat_first_piece
    })
}

fn read_mini_stream(cpf: &CompoundFileInternal, first_sector: usize, size: usize) -> Result<Vec<u8>, CpfResult> {
    assert_ne!(size, 0);

    let mini_sector_size = cpf.mini_sector_size;
    let sector_size = cpf.sector_size;
    let walk_info = WalkingInfo{
        fat: &cpf.fat,
        sector_size,
        first_directory_sector_location: 0xFFFF_FFFF, // Dummy value because we will not need it here
    };

    let mut data = Vec::<u8>::new();
    let mut bytes_remaining = size;
    let mut mini_sector_number = first_sector;
    while bytes_remaining > 0 {
        if mini_sector_number > FAT_MAX_SECTOR_NUMBER {
            return Err(CpfResult::InvalidFatOffset);
        }

        // Offset into the mini stream as if it was a linear array
        let mini_offset = mini_location_to_offset(mini_sector_number, mini_sector_size);
        // The mini stream is fragmented as regular data. We need to get
        // the actual sector in FAT.
        let nth_sector_in_fat = mini_offset / sector_size;
        let offset = sector_number_to_offset(cpf.mini_stream_first_sector, nth_sector_in_fat, &walk_info)?;

        if offset >= cpf.buffer.len() + sector_size {
            return Err(CpfResult::InvalidFatOffset);
        }

        // Sector that contais the piece of the mini stream that we need.
        let sector_data = &cpf.buffer[offset..offset+sector_size];
        // Now we need to get the data of the mini sector within the sector
        let mini_offset_in_sector = mini_offset - nth_sector_in_fat * sector_size;
        assert!(mini_offset_in_sector + mini_sector_size <= sector_data.len());

        let bytes_to_read = if bytes_remaining >= mini_sector_size { mini_sector_size } else { bytes_remaining };

        let chunk = &sector_data[mini_offset_in_sector..mini_offset_in_sector+bytes_to_read];
        data.extend(chunk);

        bytes_remaining -= bytes_to_read;
        mini_sector_number = cpf.mini_fat[mini_sector_number];
    }

    Ok(data)
}

fn read_normal_stream(cpf: &CompoundFileInternal, first_sector: usize, size: usize) -> Result<Vec<u8>, CpfResult> {
    assert_ne!(size, 0);

    let sector_size = cpf.sector_size;

    let mut data = Vec::<u8>::with_capacity(size);
    let mut bytes_remaining = size;
    let mut sector_number = first_sector;
    while bytes_remaining > 0 {
        if sector_number > FAT_MAX_SECTOR_NUMBER {
            return Err(CpfResult::InvalidFatOffset);
        }

        let offset = location_to_offset(sector_number, sector_size);
        let bytes_to_read = if bytes_remaining >= sector_size { sector_size } else { bytes_remaining };

        if offset >= cpf.buffer.len() + bytes_to_read {
            return Err(CpfResult::InvalidFatOffset);
        }

        let chunk = &cpf.buffer[offset..offset+bytes_to_read];
        data.extend(chunk);

        bytes_remaining -= bytes_to_read;
        sector_number = cpf.fat[sector_number];
    }

    assert_eq!(data.len(), size);

    Ok(data)
}

fn sector_number_to_offset(first_sector_index: usize, nth_sector: usize, walk_info: &WalkingInfo) -> Result<usize, CpfResult> {
    let mut sectors_walked = 0;
    let mut location = first_sector_index;

    while sectors_walked < nth_sector {
        if location >= walk_info.fat.len() {
            return Err(CpfResult::InvalidFatOffset);
        }

        location = walk_info.fat[location] as usize;
        if location > MAX_STREAM_ID as usize {
            return Err(CpfResult::InvalidFatOffset);
        }
        sectors_walked += 1;
    }

    Ok(location_to_offset(location, walk_info.sector_size))
}

fn version(major: u16, minor: u16) -> FileFormatVersion {
    if major == 3 && minor == 33 {
        return FileFormatVersion::Standard;
    } else if major == 3 && minor == 62 {
        return FileFormatVersion::Mso2010;
    } else if major == 4 && minor == 62 {
        return FileFormatVersion::MsnInfo;
    }

    return FileFormatVersion::Unknown;
}

pub struct CompoundFileInternal {
    pub tree: Rc<RefCell<Directory>>,
    fat: Fat,
    mini_fat: Minifat,
    mini_stream_cutoff_size: usize,
    sector_size: usize,
    mini_sector_size: usize,
    mini_stream_first_sector: usize,
    mini_stream_size: u64,
    buffer: Vec<u8>,
}

pub struct Directory {
    pub parent: Option<Weak<RefCell<Directory>>>,
    pub name: String,
    pub clsid: Clsid,
    pub subdirectories: Vec<Rc<RefCell<Directory>>>,
    pub files: Vec<File>,
    pub timestamp: u64,
}

pub struct File {
    pub parent: Weak<RefCell<Directory>>,
    pub name: String,
    pub size: u64,
    pub first_sector: usize,
}

pub fn read_buffer(buffer: Vec<u8>) -> Result<Box<CompoundFileInternal>, CpfResult> {
    if buffer.len() < size_of::<Header>() {
        return Err(CpfResult::BufferTooSmall);
    }

    let header = read_header(&buffer[0..size_of::<Header>()].try_into().unwrap())?;
    let version = version(header.major_version, header.minor_version);
    let sector_size = 2_u32.pow(header.sector_shift as u32) as usize;
    if (version == FileFormatVersion::Standard || version == FileFormatVersion::Mso2010) && sector_size != 512 {
        return Err(CpfResult::Expected512Sector);
    }
    else if version == FileFormatVersion::MsnInfo && sector_size != 4096 {
        return Err(CpfResult::Expected4096Sector);
    }
    // else: Unknown version, go along with the size info we got from the header and hope things
    // work out

    let mini_sector_size = 2_u32.pow(header.mini_sector_shift as u32) as usize;

    if (mini_sector_size > sector_size) || (sector_size % mini_sector_size != 0) {
        return Err(CpfResult::BadMiniSectorSize);
    }

    let difat = make_difat(&buffer, &header.difat_first_piece, header.first_difat_sector_location as usize, header.num_difat_sectors as usize, sector_size)?;
    let fat = make_fat(&buffer, &difat, sector_size)?;

    let walk_info = WalkingInfo{
        fat: &fat,
        sector_size,
        first_directory_sector_location: header.first_directory_sector_location,
    };

    let mini_fat = make_mini_fat(&buffer, header.first_mini_fat_sector_location as usize, &walk_info)?;

    let root_entry = read_entry(&buffer, 0, &walk_info)?;
    let tree = build_directory_tree(&buffer, &root_entry, &walk_info)?;

    Ok(Box::new(CompoundFileInternal{
        tree,
        fat,
        mini_fat,
        mini_stream_cutoff_size: header.mini_stream_cutoff_size as usize,
        sector_size,
        mini_sector_size,
        mini_stream_first_sector: root_entry.starting_sector_location as usize,
        mini_stream_size: root_entry.stream_size,
        buffer,
    }))
}

pub fn read_stream(cpf: &CompoundFileInternal, first_sector: usize, size: u64) -> Result<Vec<u8>, CpfResult> {
    if size == 0 {
        return Ok(Vec::new());
    }

    let u_size = match size.try_into() {
        Ok(usz) => usz,
        Err(_) => return Err(CpfResult::StreamTooLarge),
    };

    if u_size > cpf.mini_stream_cutoff_size {
        read_normal_stream(&cpf, first_sector, u_size)
    } else {
        read_mini_stream(&cpf, first_sector, u_size)
    }
}
