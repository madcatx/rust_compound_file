extern crate rust_compound_file;

use rust_compound_file::*;
use rust_compound_file::debug::_d_directory_tree_from_root;

use std::env;
use std::ffi::{CStr, CString};
use std::fs::File;
use std::io::prelude::*;

fn err_to_str(e: CpfResult) -> String {
    let ptr = cpf_error_to_string(e);
    unsafe {
        let c_str = CStr::from_ptr(ptr);
        String::from(c_str.to_string_lossy())
    }
}

pub fn main() {
    let args: Vec<String> = env::args().collect();

    let file_path;
    let mut stream_path = String::new();
    let mut out_file_path = String::new();

    match args.len() {
        2 => file_path = args[1].clone(),
        3 => {
            file_path = args[1].clone();
            stream_path = args[2].clone();
        },
        4 => {
            file_path = args[1].clone();
            stream_path = args[2].clone();
            out_file_path = args[3].clone();
        },
        _ => panic!("Invalid arguments passed to the program"),
    };

    let mut fh = match File::open(file_path) {
        Ok(fh) => fh,
        Err(e) => panic!("Cannot open compound file: {}", e),
    };

    let mut buffer = Vec::<u8>::new();
    match fh.read_to_end(&mut buffer) {
        Ok(_) => (),
        Err(e) => panic!("Cannot read compound file: {}", e),
    };

    let mut cpf = cpf_empty_compound_file();
    let ret = cpf_read(buffer.as_ptr(), buffer.len(), &mut cpf);
    if ret != CpfResult::Success {
        panic!("Failed to parse compound file: {}", err_to_str(ret));
    }

    if stream_path.is_empty() {
        _d_directory_tree_from_root(&cpf);
    } else {
        println!("Dumping content of file stream \"{}\"", stream_path.as_str());

        let stream_path_c_str = CString::new(stream_path).unwrap();

        let mut stream_size: u64 = 0;
        let mut ret = cpf_file_size(&cpf, stream_path_c_str.as_ptr(), &mut stream_size);
        if ret != CpfResult::Success {
            panic!("Cannot determine file stream size: {}", err_to_str(ret));
        }

        let mut payload: Vec<u8> = vec![0; stream_size as usize];
        let mut content = CpfContent{
            data: payload.as_mut_ptr(),
            size: stream_size,
        };
        ret = cpf_file_content(&cpf, stream_path_c_str.as_ptr(), &mut content);
        if ret != CpfResult::Success {
            panic!("Cannot get file content: {}", err_to_str(ret));
        }

        if out_file_path.is_empty() {
            for b in payload {
                print!("{}", b as char);
            }
        } else {
            let mut ofh = match File::create(out_file_path) {
                Ok(ofh) => ofh,
                Err(e) => panic!("Cannot open output file: {}", e),
            };

            if let Err(e) = ofh.write_all(&payload) {
                panic!("Failed to write stream content to file: {}", e);
            }
        }
    }
}
