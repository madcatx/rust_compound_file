mod compound_file_defs;
mod compound_file_impl;
pub mod debug;
mod util;

use crate::compound_file_impl::*;
use crate::util::c_str_to_string;

use std::cell::RefCell;
use std::ffi::CString;
use std::os::raw::{c_char, c_ulonglong};
use std::rc::Rc;

/// Possible return codes
#[repr(C)]
#[derive(PartialEq)]
pub enum CpfResult {
    /// Operation finished successfully
    Success,
    /// API function was called with improper parameters
    BadApiCall,
    /// Compound file has bad mini sector size
    BadMiniSectorSize,
    /// Bad C string was passed to the API.
    BadString,
    /// Compound file has incorrect signature
    BadSignature,
    /// Buffer was to small perform requested operation
    BufferTooSmall,
    /// Compound files stored as big endian are not supported
    BigEndianNotSupported,
    /// Conversion from "byte" to "short" has failed
    CannotConvertU8ToU16,
    /// Compound file has corrupted DIFAT table
    DifatCorrupted,
    /// Length of an entry object name takes up odd number of bytes
    EntryNameHasOddLength,
    /// Reported length of an entry object name is too long
    EntryNameTooLong,
    /// Compound file should have 512-byte sectors but it does not
    Expected512Sector,
    /// Compound file should have 4096-byte sector but it does not
    Expected4096Sector,
    /// Attempted to use invalid offset into DIFAT table
    InvalidDifatOffset,
    /// Encountered entry object whose type was invalid in the given context
    InvalidEntryObjectType,
    /// Attempted to use invalid object into FAT table;
    InvalidFatOffset,
    /// Invalid path to stream
    InvalidPath,
    /// Compound file contains invalid UTF-16 string
    InvalidUtf16String,
    /// Attempted to query an entry that does not exist in the compound file
    NoSuchEntry,
    /// Compound file specifies unknown byte order
    UnknownByteOrder,
    /// Encountered entry object with unknown color flag
    UnknownEntryObjectColor,
    /// Encountered entry object with unknown type
    UnknownEntryObjectType,
    /// Attempted to use an array slice with wrong length
    WrongSliceLength,
    /// Stream is too large to be processed
    StreamTooLarge,
}

/// CLSID
pub type Clsid = [u8; 16];

/// Buffer object used to retrieve content of a file (aka StreamObject) stored in the compound file.
/// Buffer and its size must be initialized by the caller of `cpf_file_content()`.
#[repr(C)]
pub struct CpfContent {
    /// Array of bytes that must be at least as large as the content being retrieved. The array
    /// must be initialized by the caller.
    pub data: *mut u8,
    /// Size of the `data` array.
    pub size: c_ulonglong,
}

/// Handle to the compound file.
#[repr(C)]
pub struct CpfCompoundFile {
    data: *mut CompoundFileInternal,
}

/// List of C strings. Used to return list of directories or files in the compound file.
/// The object is managed by the library. `cpf_release_string_list()` must be used to release
/// resources used by the object.
#[repr(C)]
pub struct CpfStringList {
    /// Array of UTF-8 encoded C-strings
    pub strings: *mut *mut c_char,
    /// Size of the `strings` array
    pub size: c_ulonglong,
}

type DirectoryRef = Rc<RefCell<Directory>>;

static CPF_ERR_SUCCESS: &'static str = concat!("No error occured", '\0');
static CPF_ERR_BAD_API_CALL: &'static str = concat!("API library was not called correctly", '\0');
static CPF_ERR_BAD_MINI_SECTOR_SIZE: &'static str = concat!("Reported mini sector size is nonsensical", '\0');
static CPF_ERR_BAD_SIGNATURE: &'static str = concat!("File has a bad signature. It is either not a compound file or the file is corrupted", '\0');
static CPF_ERR_BUFFER_TOO_SMALL: &'static str = concat!("Passed buffer was too small to perform the requested operation", '\0');
static CPF_ERR_BIG_ENDIAN_NOT_SUPPORTED: &'static str = concat!("The library currently expects little-endian file format", '\0');
static CPF_ERR_CANNOT_CONVERT_U8_TO_U16: &'static str = concat!("Conversion from 1-byte to 2-byte array failed", '\0');
static CPF_ERR_DIFAT_CORRUPTED: &'static str = concat!("DIFAT table seems corrupted", '\0');
static CPF_ERR_ENTRY_NAME_HAS_ODD_LENGTH: &'static str = concat!("Number of bytes used for the name of an Entry object is odd", '\0');
static CPF_ERR_ENTRY_NAME_TOO_LONG: &'static str = concat!("Reported number of bytes used for the name of an Entry object exceeds the length of the name array", '\0');
static CPF_ERR_EXPECTED_512_SECTOR: &'static str = concat!("Detected compound file version should have 512-byte sector size but it reports a different size", '\0');
static CPF_ERR_EXPECTED_4096_SECTOR: &'static str = concat!("Detected compound file version should have 4096-byte sector size but it reports a different size", '\0');
static CPF_ERR_INVALID_DIFAT_OFFSET: &'static str = concat!("Encoountered invalid DIFAT table offset", '\0');
static CPF_ERR_INVALID_ENTRY_OBJECT_TYPE: &'static str = concat!("Entry object type was invalid in the given context", '\0');
static CPF_ERR_INVALID_FAT_OFFSET: &'static str = concat!("Encountered invalid FAT table offset", '\0');
static CPF_ERR_INVALID_PATH: &'static str = concat!("Invalid path was passed to a function", '\0');
static CPF_ERR_INVALID_UTF16_STRING: &'static str = concat!("Failed to convert a byte array to UTF-16 string", '\0');
static CPF_ERR_NO_SUCH_ENTRY: &'static str = concat!("Compound file does not contain the requested item", '\0');
static CPF_ERR_UNKNOWN_BYTE_ORDER: &'static str = concat!("Compound file specifies unknown byte order", '\0');
static CPF_ERR_UNKNOWN_ENTRY_OBJECT_COLOR: &'static str = concat!("Entry object has invalid color flag", '\0');
static CPF_ERR_UNKNOWN_ENTRY_OBJECT_TYPE: &'static str = concat!("Entry object has unknown type", '\0');
static CPF_ERR_WRONG_SLICE_LENGTH: &'static str = concat!("Array slice had wrong length to perform the requested operation", '\0');
static CPF_ERR_STREAM_TOO_LARGE: &'static str = concat!("Stream is too large and cannot be processed", '\0');
static CPF_ERR__UNKNOWN_ERROR: &'static str = concat!("Unknown error", '\0');

fn find_directory(cpf_data: &CompoundFileInternal, path: &[String]) -> Result<DirectoryRef, CpfResult> {
    assert!(path.len() > 0);

    let dir = if path.len() == 1 {
        cpf_data.tree.clone()
    } else {
        find_subdirectory(cpf_data.tree.clone(), &path[1], &path[2..])?
    };

    Ok(dir)
}

fn find_subdirectory(dir: DirectoryRef, name: &String, path: &[String]) -> Result<DirectoryRef, CpfResult> {
    for subdir in &dir.borrow().subdirectories {
        if subdir.borrow().name == *name {
            if path.is_empty() {
                return Ok(subdir.clone());
            } else {
                return find_subdirectory(subdir.clone(), &path[0], &path[1..]);
            }
        }
    }

    Err(CpfResult::InvalidPath)
}

fn tokenize_path(ptr: *const c_char) -> Result<Vec<String>, CpfResult> {
    let path = c_str_to_string(ptr)?;
    if path.is_empty() {
        return Err(CpfResult::InvalidPath);
    }
    if !path.starts_with("/") {
        return Err(CpfResult::InvalidPath);
    }

    if path.len() == 1 {
        return Ok(vec![String::new()]);
    }

    let fixed_path = if path.ends_with("/") {
        &path[0..path.len()-1]
    } else {
        &path
    };

    let toks: Vec<String> = fixed_path.split("/").into_iter().map(|s| String::from(s)).collect();
    assert!(toks.len() > 1);

    Ok(toks)
}

fn vec_to_string_list(mut strings: Vec<*mut c_char>, list: *mut CpfStringList) -> CpfResult {
    strings.shrink_to_fit(); // Required so that we can release the vec content without leaking
    let list_size: c_ulonglong = match strings.len().try_into() {
        Ok(sz) => sz,
        Err(_) => {
            for s in strings {
                unsafe { drop(CString::from_raw(s)); }
            }
            return CpfResult::StreamTooLarge;
        }
    };
    let ptr = strings.as_mut_ptr();
    unsafe {
        (*list).strings = ptr;
        (*list).size = list_size;
    }

    std::mem::forget(strings);

    CpfResult::Success
}

/// Returns an empty `CpfCompoundFile`.
#[no_mangle]
pub extern "system" fn cpf_empty_compound_file() -> CpfCompoundFile {
    CpfCompoundFile{
        data: std::ptr::null_mut(),
    }
}

/// Returns an empty `CpfStringList`.
#[no_mangle]
pub extern "system" fn cpf_empty_string_list() -> CpfStringList {
    CpfStringList{
        strings: std::ptr::null_mut(),
        size: 0,
    }
}

/// Translates `CpfResult` return code to textual description.
///
/// Text description is a pointer to statically allocated UTF-8 C-string.
#[no_mangle]
pub extern "system" fn cpf_error_to_string(result: CpfResult) -> *const c_char {
    match result {
        CpfResult::Success => CPF_ERR_SUCCESS,
        CpfResult::BadApiCall => CPF_ERR_BAD_API_CALL,
        CpfResult::BadSignature => CPF_ERR_BAD_SIGNATURE,
        CpfResult::BufferTooSmall => CPF_ERR_BUFFER_TOO_SMALL,
        CpfResult::BigEndianNotSupported => CPF_ERR_BIG_ENDIAN_NOT_SUPPORTED,
        CpfResult::CannotConvertU8ToU16 => CPF_ERR_CANNOT_CONVERT_U8_TO_U16,
        CpfResult::DifatCorrupted => CPF_ERR_DIFAT_CORRUPTED,
        CpfResult::EntryNameHasOddLength => CPF_ERR_ENTRY_NAME_HAS_ODD_LENGTH,
        CpfResult::EntryNameTooLong => CPF_ERR_ENTRY_NAME_TOO_LONG,
        CpfResult::Expected512Sector => CPF_ERR_EXPECTED_512_SECTOR,
        CpfResult::Expected4096Sector => CPF_ERR_EXPECTED_4096_SECTOR,
        CpfResult::InvalidDifatOffset => CPF_ERR_INVALID_DIFAT_OFFSET,
        CpfResult::InvalidEntryObjectType => CPF_ERR_INVALID_ENTRY_OBJECT_TYPE,
        CpfResult::InvalidFatOffset => CPF_ERR_INVALID_FAT_OFFSET,
        CpfResult::InvalidPath => CPF_ERR_INVALID_PATH,
        CpfResult::InvalidUtf16String => CPF_ERR_INVALID_UTF16_STRING,
        CpfResult::NoSuchEntry => CPF_ERR_NO_SUCH_ENTRY,
        CpfResult::UnknownByteOrder => CPF_ERR_UNKNOWN_BYTE_ORDER,
        CpfResult::UnknownEntryObjectColor => CPF_ERR_UNKNOWN_ENTRY_OBJECT_COLOR,
        CpfResult::UnknownEntryObjectType => CPF_ERR_UNKNOWN_ENTRY_OBJECT_TYPE,
        CpfResult::WrongSliceLength => CPF_ERR_WRONG_SLICE_LENGTH,
        CpfResult::StreamTooLarge => CPF_ERR_STREAM_TOO_LARGE,
        CpfResult::BadMiniSectorSize => CPF_ERR_BAD_MINI_SECTOR_SIZE,
        _ => CPF_ERR__UNKNOWN_ERROR,
    }.as_ptr() as *const c_char
}

/// Retrieves content of a file (aka StreamObject)
///
/// Arguments:
///
///  * `cpf`: Compound file handle
///  * `path`: Path to the file. The path must be an UTF-8 C-string formatted like this:
///            /Subdir1/Subdir2/AFile
///  * `content`: Content buffer. The buffer must be initialized by the called. See `CpfContent`
///               documentation for details. Content of the buffer is not modified unless the
///               function returns `CpfResult::Success`
#[no_mangle]
pub extern "system" fn cpf_file_content(cpf: *const CpfCompoundFile, path: *const c_char, content: *mut CpfContent) -> CpfResult {
    let cpf_data = unsafe {
        if (*cpf).data.is_null() {
            return CpfResult::BadApiCall;
        }

        (*cpf).data.as_ref().unwrap()
    };

    let toks_path = match tokenize_path(path) {
        Ok(toks) => toks,
        Err(e) => return e,
    };
    if toks_path.len() < 2 {
        return CpfResult::InvalidPath;
    }

    let dir = match find_directory(cpf_data, &toks_path[0..toks_path.len() - 1]) {
        Ok(dir) => dir,
        Err(e) => return e,
    };
    let file_name = toks_path.last().unwrap();

    for file in &dir.borrow().files {
        if file.name == *file_name {
            unsafe {
                if (*content).size < file.size {
                    return CpfResult::BufferTooSmall;
                }
            }

            let stream = match read_stream(cpf_data, file.first_sector, file.size) {
                Ok(stream) => stream,
                Err(e) => return e,
            };

            unsafe {
                std::ptr::copy_nonoverlapping(stream.as_ptr(), (*content).data, stream.len());
            }

            return CpfResult::Success;
        }
    }

    CpfResult::NoSuchEntry
}

/// Returns the size of a file (aka StreamObject)
///
/// Arguments:
///
///  * `cpf`: Compound file handle
///  * `path`: Path to the file. The path must be an UTF-8 C-string formatted like this:
///            /Subdir1/Subdir2/AFile
///  * `size`: Size of the file in bytes. This value is not modified unless the function returns
///            `CpfResult::Success`
#[no_mangle]
pub extern "system" fn cpf_file_size(cpf: *const CpfCompoundFile, path: *const c_char, size: *mut c_ulonglong) -> CpfResult {
    let cpf_data = unsafe {
        if (*cpf).data.is_null() {
            return CpfResult::BadApiCall;
        }

        (*cpf).data.as_ref().unwrap()
    };

    let toks_path = match tokenize_path(path) {
        Ok(toks) => toks,
        Err(e) => return e,
    };
    if toks_path.len() < 2 {
        return CpfResult::InvalidPath;
    }

    let dir = match find_directory(cpf_data, &toks_path[0..toks_path.len()-1]) {
        Ok(dir) => dir,
        Err(e) => return e,
    };
    let file_name = toks_path.last().unwrap();
    for file in &dir.borrow().files {
        if file.name == *file_name {
            let c_size: c_ulonglong = match file.size.try_into() {
                Ok(sz) => sz,
                Err(_) => return CpfResult::StreamTooLarge,
            };
            unsafe {
                *size = c_size;
            }
            return CpfResult::Success;
        }
    }

    CpfResult::NoSuchEntry
}

/// Returns list of files in a given directory
///
/// Arguments:
///
///  * `cpf`: Compound file handle
///  * `path`: Path to the file. The path must be UTF-8 C string formatted like this:
///            /Subdir1/Subdir2/AFile
///  * `list_of_files`: List of files in the directory filled out by this function. The function
///                     must receive an empty list created by `cpf_empty_string_list()`.
///                     Content of `list_of_files` is not modified if the function does not
///                     return `CpfResult::Success`.
#[no_mangle]
pub extern "system" fn cpf_list_files(cpf: *const CpfCompoundFile, path: *const c_char, list_of_files: *mut CpfStringList) -> CpfResult {
    unsafe {
        if (*list_of_files).strings != std::ptr::null_mut() {
            return CpfResult::BadApiCall;
        }
    }

    let cpf_data = unsafe {
        if (*cpf).data.is_null() {
            return CpfResult::BadApiCall;
        }

        (*cpf).data.as_ref().unwrap()
    };

    let toks_path = match tokenize_path(path) {
        Ok(toks) => toks,
        Err(e) => return e,
    };

    let dir = match find_directory(cpf_data, &toks_path) {
        Ok(dir) => dir,
        Err(e) => return e,
    };

    let list: Vec<*mut c_char> = dir.borrow().files.iter().map(|f| {
        let c_str = CString::new(f.name.as_str()).unwrap();
        c_str.into_raw()
    }).collect();

    vec_to_string_list(list, list_of_files)
}

/// Returns list of subdirectories in a given directory.
///
/// Arguments:
///
///  * `cpf`: Compound file handle
///  * `path`: Path to the directory. The path must be UTF-8 C-string formatted like this:
///            /Subdir1/Subdir2
///  * `list_of_subdirs`: List of subdirectories in the directory filled out by this function. The
///                       function must receive an empty list created by `cpf_empty_string_list()`.
///                       Content of `list_of_subdirs` is not modified if the function does not
///                       return `CpfResult::Success`.
#[no_mangle]
pub extern "system" fn cpf_list_subdirectories(cpf: *const CpfCompoundFile, path: *const c_char, list_of_subdirs: *mut CpfStringList) -> CpfResult {
    unsafe {
        if (*list_of_subdirs).strings != std::ptr::null_mut() {
            return CpfResult::BadApiCall;
        }
    }

    let cpf_data = unsafe {
        if (*cpf).data.is_null() {
            return CpfResult::BadApiCall;
        }

        (*cpf).data.as_ref().unwrap()
    };

    let toks_path = match tokenize_path(path) {
        Ok(toks) => toks,
        Err(e) => return e,
    };
    let dir = match find_directory(cpf_data, &toks_path) {
        Ok(dir) => dir,
        Err(e) => return e,
    };

    let list: Vec<*mut c_char> = dir.borrow().subdirectories.iter().map(|subdir| {
        let c_str = CString::new(subdir.borrow().name.as_str()).unwrap();
        c_str.into_raw()
    }).collect();

    vec_to_string_list(list, list_of_subdirs)
}

/// Reads a compound file from an array of bytes
///
/// Arguments:
///
///  * `data`: Data to read as compound file
///  * `size`: Size of the `data` array
///  * `cpf`: `CpfCompoundFile` handle to initialize. The handle must be an empty handle created by
///           `cpf_empty_compound_file()`
#[no_mangle]
pub extern "system" fn cpf_read(data: *const u8, size: usize, cpf: *mut CpfCompoundFile) -> CpfResult {
    unsafe {
        if !(*cpf).data.is_null() {
            return CpfResult::BadApiCall;
        }
    }

    let buffer = unsafe {
        let slc = std::slice::from_raw_parts(data, size);
        Vec::from(slc)
    };

    let cpfile = match read_buffer(buffer) {
        Ok(cpfile) => cpfile,
        Err(e) => return e,
    };

    unsafe {
        (*cpf).data = Box::into_raw(cpfile);
    }

    CpfResult::Success
}

/// Releases resources used by `CpfCompoundFile`
///
/// Arguments:
///
///  * `cpf`: The file handle to release
#[no_mangle]
pub extern "system" fn cpf_release_compound_file(cpf: *mut CpfCompoundFile) {
    unsafe {
        if !(*cpf).data.is_null() {
            drop(Box::from_raw((*cpf).data));
            (*cpf).data = std::ptr::null_mut();
        }
    }
}

/// Releases resources used by `CpfStringList`
///
/// Arguments:
///
///  * `list`: The list to release
#[no_mangle]
pub extern "system" fn cpf_release_string_list(list: *mut CpfStringList) {
    unsafe {
        if !(*list).strings.is_null() {
            let vec = Vec::from_raw_parts((*list).strings, (*list).size as usize, (*list).size as usize);

            for s in vec {
                drop(CString::from_raw(s));
            }

            (*list).strings = std::ptr::null_mut();
        }
    }
}
