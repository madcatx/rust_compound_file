use crate::Clsid;
use crate::CpfCompoundFile;
use crate::compound_file_defs::{Entry, EntryObjectType, FAT_SECTOR_END_OF_CHAIN};
use crate::compound_file_impl::Directory;

use std::cell::RefCell;
use std::rc::Rc;

fn _printable_file_name(name: &String) -> String {
    let mut printable = String::new();

    for ch in name.as_bytes() {
        if *ch < 0x20 {
            printable.push_str(format!("{:#04x}", *ch).as_str());
        } else {
            printable.push(*ch as char);
        }
    }

    printable
}

pub fn _d_clsid_to_string(clsid: &Clsid) -> String {
    let mut str = String::from("{");

    for idx in 0..4 {
        str += format!("{:02X}", clsid[idx]).as_str();
    }
    str += "-";

    for idx in 4..6 {
        str += format!("{:02X}", clsid[idx]).as_str();
    }
    str += "-";

    for idx in 6..8 {
        str += format!("{:02x}", clsid[idx]).as_str();
    }
    str += "-";

    for idx in 8..10 {
        str += format!("{:02X}", clsid[idx]).as_str();
    }
    str += "-";

    for idx in 10..16 {
        str += format!("{:02X}", clsid[idx]).as_str();
    }

    str + "}"
}

pub fn _d_directory_entry(dirent: &Entry, path: String) {
    println!("Path: {}", path);

    let name = String::from_utf16(&dirent.name).unwrap();
    print!("Name: {} (", _printable_file_name(&name));

    println!("Object type: {}", match dirent.object_type {
        EntryObjectType::UnknownUnallocated => "Unknown/unallocated",
        EntryObjectType::StorageObject => "Storage",
        EntryObjectType::StreamObject => "Stream",
        EntryObjectType::RootStorageObject=> "Root",
    });
    println!("Left: {}", dirent.left_sibling_id);
    println!("Right: {}", dirent.right_sibling_id);
    println!("Child: {}", dirent.child_id);
    println!("Starting sector loc: {}", dirent.starting_sector_location);
    println!("Stream size: {}", dirent.stream_size);
}

pub fn _d_directory_entry_2(dirent: &Entry) {
    println!("Name: {}", String::from_utf16(&dirent.name).unwrap());
    println!("Left: {}", dirent.left_sibling_id);
    println!("Right: {}", dirent.right_sibling_id);
    println!("Child: {}", dirent.child_id);
}

pub fn _d_directory_tree(dir: Rc<RefCell<Directory>>, level: usize) -> () {
    let padding = format!("{0:1$}", "", level * 2);
    println!("{padding}{name} {clsid}", name=dir.borrow().name, clsid=_d_clsid_to_string(&dir.borrow().clsid));

    println!("{padding} Parent: {}", match &dir.borrow().parent {
        Some(p) => match &p.upgrade() {
            Some(up) => up.borrow().name.clone(),
            None => String::from("(None)"),
        },
        None => String::from("(None)"),
    });
    println!("{padding}  Files:");
    for f in &dir.borrow().files {
        println!("{padding}    {} ({} bytes)", _printable_file_name(&f.name), f.size);
    }

    println!("{padding}  Subdirectories:");
    for subdir in &dir.borrow().subdirectories {
        _d_directory_tree(subdir.clone(), level + 2);
    }
    println!("");
}

pub fn _d_directory_tree_from_root(cpf: &CpfCompoundFile) -> () {
    let root = unsafe {
        (*cpf.data).tree.clone()
    };

    _d_directory_tree(root, 0);
}

pub fn _d_sector_chain(fat: &Vec<usize>, start: usize) {
    let mut chain = vec![start];

    let mut next = fat[start];
    while next != FAT_SECTOR_END_OF_CHAIN {
        chain.push(next);
        next = fat[next as usize];
    }

    println!("{}", chain.iter().map(|x| x.to_string()).collect::<Vec<String>>().join(", "));
}
